/**
 * 
 */
package org.xistorias;

/**
 * Definition of a Streaming profile.
 * @author Miguel Gomes (mdg@uninova.pt)
 * @since 17/12/2014 16:06:14
 *
 */
public class Profile
{
	/** Profile name */
	public String profileName;
	/** Whether or not video is enabled */
	public boolean video_enabled;
	/** Video Aspect Ratio */
	public String video_aspectRatio;
	/** Video Resolution width */
	public int video_resolution_width;
	/** Video Resolution height */
	public int video_resolution_height;
	/** Video target bitrate */
	public int video_bitrate;
	/** Video target framerate */
	public float video_framerate;
	/** Video Codec name */
	public String video_codec;
	/** Whether or not audio is enabled */
	public boolean audio_enabled;
	/** Audio Codec name */
	public String audio_codec;
	/** Audio target bitrate */
	public int audio_bitrate;
	/** Number of audio channels */
	public int audio_channels;
	/** Audio sample rate */
	public int audio_samplerate;

	/**
	 * Creates a new Profile.
	 */
	public Profile()
	{
		this.profileName = "unnamed";
		this.video_enabled = true;
		this.video_codec = "h264";
		this.video_aspectRatio = "4:3";
		this.video_resolution_width = 320;
		this.video_resolution_height = 240;
		this.video_bitrate = 300;
		this.video_framerate = 25;
		this.audio_enabled = true;
		this.audio_codec = "mp3";
		this.audio_bitrate = 96;
		this.audio_channels = 2;
		this.audio_samplerate = 44100;
	}

	/**
	 * Creates a new named profile.
	 * @param name Profile name
	 */
	public Profile(String name) {
		this();
		this.profileName = name;
	}

	/**
	 * Gets the transcode string.
	 * @return Transcode string
	 */
	public String getTranscodeString() {
		return getTranscodeString(true);
	}

	/**
	 * Gets the transcode string.
	 * @param compensate Whether or not the lack of audio stream should be compensated (default: true)
	 * @return Transcode string
	 */
	public String getTranscodeString(boolean compensate)
	{
		String video = "";
		String audio = "";
		if (this.video_enabled) {
			video = video + "vcodec=" + this.video_codec + ",vb=" + this.video_bitrate + ((!this.audio_enabled) && (compensate) ? this.audio_bitrate : 0);
			if (video_codec == "h264")
				video += ",venc=x264{aud,profile=baseline,level=30,keyint=30,ref=1}";
			video += ",fps=" + this.video_framerate + ",scale=1,width=" + this.video_resolution_width + ",height=" + this.video_resolution_height;
		}
		if (this.audio_enabled)
			audio = audio + "acodec=" + this.audio_codec + ",ab=" + this.audio_bitrate + ",channels=" + this.audio_channels + ",samplerate=" + this.audio_samplerate;
		return "#transcode{" + video + ((this.video_enabled) && (this.audio_enabled) ? "," : "") + audio + "}";
	}
}
