/**
 * 
 */
package org.xistorias.gui;

import org.xistorias.Profile;

import uk.co.caprica.vlcj.binding.LibVlc;
import uk.co.caprica.vlcj.medialist.MediaList;
import uk.co.caprica.vlcj.medialist.MediaListItem;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.discoverer.MediaDiscoverer;
import uk.co.caprica.vlcj.runtime.RuntimeUtil;

import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;

import de.humatic.dsj.DSCapture;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.OperationsException;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * Startup Screen
 * @author Miguel Gomes (mdg@uninova.pt)
 * @since 17/12/2014 16:04:46
 *
 */
public class StartupScreen extends JFrame
{
	private static final long serialVersionUID = -5552457316855863701L;
	private String vlcPath;
	private HashMap<String, Profile> profiles;
	private String currentPresent;
	private String currentAr;
	private String currentResolution;
	private Image appIcon;
	private JLabel jLabel1;
	private JLabel jLabel10;
	private JLabel jLabel11;
	private JLabel jLabel12;
	private JLabel jLabel13;
	private JLabel jLabel14;
	private JLabel jLabel15;
	private JLabel jLabel16;
	private JLabel jLabel17;
	private JLabel jLabel18;
	private JLabel jLabel19;
	private JLabel jLabel2;
	private JLabel jLabel20;
	private JLabel jLabel21;
	private JLabel jLabel3;
	private JLabel jLabel4;
	private JLabel jLabel5;
	private JLabel jLabel6;
	private JLabel jLabel7;
	private JLabel jLabel8;
	private JLabel jLabel9;
	private JPanel jPanel1;
	private JButton jb_start_broadcast;
	private JButton jb_vlc_path;
	private JComboBox jcb_audio_bitrate;
	private JComboBox jcb_audio_channels;
	private JCheckBox jcb_audio_compensate;
	private JComboBox jcb_audio_device;
	private JComboBox jcb_audio_sample_rate;
	private JComboBox jcb_presents;
	private JComboBox jcb_video_ar;
	private JComboBox jcb_video_codec;
	private JComboBox jcb_video_device;
	private JComboBox jcb_video_framerate;
	private JComboBox jcb_video_resolution_presents;
	private JCheckBox jck_video_duplicate;
	private JSpinner js_video_bitrate;
	private JTextField jtf_output_field;
	private JTextField jtf_video_height;
	private JTextField jtf_video_width;

	private List<String> videoDeviceNames;
	private List<String> audioDeviceNames;
	private OperatingSystem os;
	private boolean hasVlcLib;
	private boolean duplicateDisplay;

	public enum DeviceType {
		AUDIO,
		VIDEO;
	}

	public enum OperatingSystem {
		UNKNOWN,
		WINDOWS,
		LINUX,
		OSX
	}

	public StartupScreen() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException
	{
		this(null);
	}

	public StartupScreen(String vlcPath) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException
	{
		os = getOperatingSystem();
		try
		{
			// Check what is the System look and feel
			String crossLaf = UIManager.getCrossPlatformLookAndFeelClassName();
			String sysLaf = UIManager.getSystemLookAndFeelClassName();
			String laf = sysLaf;
			if (sysLaf.equals(crossLaf)) { // try again
				String nimbusLaf = null;
				String gtkLaf = null;
				String windowsLaf = null;
				for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
					if (info.getName().equals("Nimbus")) {
						nimbusLaf = info.getClassName();
					} else if (info.getName().equals("GTK+")) {
						gtkLaf = info.getClassName();
					} else if (info.getName().equals("Windows")) {
						windowsLaf = info.getClassName();
					}
			    }
				switch (os) {
					case WINDOWS: laf = (windowsLaf != null ? windowsLaf : (nimbusLaf != null ? nimbusLaf : sysLaf)); break;
					case LINUX: laf = (gtkLaf != null ? gtkLaf : (nimbusLaf != null ? nimbusLaf : sysLaf)); break;
					case OSX:
					default:
						laf = (nimbusLaf != null ? nimbusLaf : sysLaf); break;
				}
			}
			UIManager.setLookAndFeel(laf); 
		} catch (Exception localException) {
			System.err.println("Exception: "+localException.getLocalizedMessage());
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName()); 
		}
		this.hasVlcLib = false;
		if (vlcPath == null) {
			JOptionPane.showMessageDialog(new JDialog(), 
					"The path to the VLC executable is not set, please click the VLC button on the next window to set the path to the executable.", 
					"VLC not found", 
					0);
		}
		this.vlcPath = vlcPath;
		loadVLClib();
		try {
			this.appIcon = new ImageIcon(StartupScreen.class.getResource("app_icon.png")).getImage();
		} catch (NullPointerException e) {
			System.err.println("Could not find the Application Icon");
		}
		videoDeviceNames = new ArrayList<String>();
		audioDeviceNames = new ArrayList<String>();
		duplicateDisplay = false;
		initDevices();
		initComponents();
		initProfiles();
		this.currentPresent = this.jcb_presents.getSelectedItem().toString();
		setPresent(this.currentPresent, true);
		this.currentAr = this.jcb_video_ar.getSelectedItem().toString();
		this.currentResolution = this.jcb_video_resolution_presents.getSelectedItem().toString();
	}

	private OperatingSystem getOperatingSystem() {
		String OS = System.getProperty("os.name").toLowerCase();
		if (OS.indexOf("windows") >= 0) return OperatingSystem.WINDOWS;
		else if (OS.indexOf("mac os x") >= 0) return OperatingSystem.OSX;
		else if (OS.indexOf("linux") >= 0) return OperatingSystem.LINUX;
		return OperatingSystem.UNKNOWN;
	}

	private List<String> getDeviceNames(DeviceType type) {
		ArrayList<String> devNames = new ArrayList<String>();
		boolean hasFailed = true;
		if (hasVlcLib && os != OperatingSystem.WINDOWS) {
			MediaPlayerFactory mpf = new MediaPlayerFactory();
			try {
				if (type == DeviceType.VIDEO) {
					MediaDiscoverer vd = mpf.newVideoMediaDiscoverer();
					MediaList ml = vd.getMediaList();
					for (MediaListItem dev: ml.items()) {
						System.out.println("vlc video dev: "+dev.name());
						devNames.add(dev.name());
					}
					hasFailed=false;
				} else if (type == DeviceType.AUDIO) {
					MediaDiscoverer vd = mpf.newAudioMediaDiscoverer();
					MediaList ml = vd.getMediaList();
					for (MediaListItem dev: ml.items()) {
						System.out.println("vlc audio dev: "+dev.name());
						devNames.add(dev.name());
					}
					hasFailed=false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				hasFailed = true;
			}
		} else if (os == OperatingSystem.WINDOWS) {
			try {
				if (type == DeviceType.VIDEO) {
					de.humatic.dsj.DSFilterInfo[][] devices = de.humatic.dsj.DSCapture.queryDevices(0);
					for (de.humatic.dsj.DSFilterInfo device: devices[0]) {
						System.out.println("dsj video dev: "+device.getName());
						devNames.add(device.getName());
					}
				} else if (type == DeviceType.AUDIO) {
					de.humatic.dsj.DSFilterInfo[][] devices = de.humatic.dsj.DSCapture.queryDevices(0);
					for (de.humatic.dsj.DSFilterInfo device: devices[1]) {
						System.out.println("dsj audio dev: "+device.getName());
						devNames.add(device.getName());
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
				hasFailed = true;
			}
		}
		return devNames;
	}

	private void initDevices() {
		this.videoDeviceNames = getDeviceNames(DeviceType.VIDEO);
		this.audioDeviceNames = getDeviceNames(DeviceType.AUDIO);
	}



	private void initProfiles() {
		this.profiles = new HashMap<String, Profile>();

		Profile profile = new Profile("Lowest SD Quality 4:3");
		profile.video_aspectRatio = "4:3";
		profile.video_bitrate = 200;
		profile.video_framerate = 15;
		profile.video_resolution_width = 320;
		profile.video_resolution_height = 240;
		profile.audio_channels = 1;
		profile.audio_bitrate = 32;
		profile.audio_samplerate = 22050;
		this.profiles.put(profile.profileName, profile);

		profile = new Profile("Lowest SD Quality 16:9");
		profile.video_aspectRatio = "16:9";
		profile.video_bitrate = 200;
		profile.video_framerate = 15;
		profile.video_resolution_width = 352;
		profile.video_resolution_height = 198;
		profile.audio_channels = 1;
		profile.audio_bitrate = 32;
		profile.audio_samplerate = 22050;
		this.profiles.put(profile.profileName, profile);

		profile = new Profile("Basic SD Quality 4:3");
		profile.video_aspectRatio = "4:3";
		profile.video_bitrate = 300;
		profile.video_framerate = 20;
		profile.video_resolution_width = 320;
		profile.video_resolution_height = 240;
		profile.audio_channels = 1;
		profile.audio_bitrate = 56;
		profile.audio_samplerate = 22050;
		this.profiles.put(profile.profileName, profile);

		profile = new Profile("Basic SD Quality 16:9");
		profile.video_aspectRatio = "16:9";
		profile.video_bitrate = 300;
		profile.video_framerate = 20;
		profile.video_resolution_width = 352;
		profile.video_resolution_height = 198;
		profile.audio_channels = 1;
		profile.audio_bitrate = 56;
		profile.audio_samplerate = 22050;
		this.profiles.put(profile.profileName, profile);

		profile = new Profile("Standard SD Quality 4:3");
		profile.video_aspectRatio = "4:3";
		profile.video_bitrate = 350;
		profile.video_framerate = 25;
		profile.video_resolution_width = 320;
		profile.video_resolution_height = 240;
		profile.audio_channels = 2;
		profile.audio_bitrate = 96;
		profile.audio_samplerate = 22050;
		this.profiles.put(profile.profileName, profile);

		profile = new Profile("Standard SD Quality 16:9");
		profile.video_aspectRatio = "16:9";
		profile.video_bitrate = 350;
		profile.video_framerate = 25;
		profile.video_resolution_width = 352;
		profile.video_resolution_height = 198;
		profile.audio_channels = 2;
		profile.audio_bitrate = 96;
		profile.audio_samplerate = 22050;
		this.profiles.put(profile.profileName, profile);

		profile = new Profile("High SD Quality 4:3");
		profile.video_aspectRatio = "4:3";
		profile.video_bitrate = 500;
		profile.video_framerate = 30;
		profile.video_resolution_width = 320;
		profile.video_resolution_height = 240;
		profile.audio_channels = 2;
		profile.audio_bitrate = 128;
		profile.audio_samplerate = 44100;
		this.profiles.put(profile.profileName, profile);

		profile = new Profile("High SD Quality 16:9");
		profile.video_aspectRatio = "16:9";
		profile.video_bitrate = 500;
		profile.video_framerate = 30;
		profile.video_resolution_width = 352;
		profile.video_resolution_height = 198;
		profile.audio_channels = 2;
		profile.audio_bitrate = 128;
		profile.audio_samplerate = 44100;
		this.profiles.put(profile.profileName, profile);

		profile = new Profile("Best SD Quality 4:3");
		profile.video_aspectRatio = "4:3";
		profile.video_bitrate = 600;
		profile.video_framerate = 30;
		profile.video_resolution_width = 640;
		profile.video_resolution_height = 480;
		profile.audio_channels = 2;
		profile.audio_bitrate = 128;
		profile.audio_samplerate = 44100;
		this.profiles.put(profile.profileName, profile);

		profile = new Profile("Best SD Quality 16:9");
		profile.video_aspectRatio = "16:9";
		profile.video_bitrate = 600;
		profile.video_framerate = 30;
		profile.video_resolution_width = 720;
		profile.video_resolution_height = 405;
		profile.audio_channels = 2;
		profile.audio_bitrate = 128;
		profile.audio_samplerate = 44100;
		this.profiles.put(profile.profileName, profile);
	}

	private void initComponents()
	{

		this.jLabel1 = new JLabel();
		this.jPanel1 = new JPanel();
		this.jLabel2 = new JLabel();
		this.jcb_presents = new JComboBox();
		this.jLabel3 = new JLabel();
		this.jLabel4 = new JLabel();
		this.jcb_video_ar = new JComboBox();
		this.jLabel5 = new JLabel();
		this.jcb_video_resolution_presents = new JComboBox();
		this.jLabel6 = new JLabel();
		this.jtf_video_width = new JTextField();
		this.jLabel7 = new JLabel();
		this.jtf_video_height = new JTextField();
		this.jLabel8 = new JLabel();
		this.jLabel9 = new JLabel();
		this.jLabel10 = new JLabel();
		this.jcb_video_device = new JComboBox();
		this.jLabel11 = new JLabel();
		this.jcb_video_framerate = new JComboBox();
		this.jLabel12 = new JLabel();
		this.jcb_video_codec = new JComboBox();
		this.jLabel13 = new JLabel();
		this.jLabel14 = new JLabel();
		this.jLabel15 = new JLabel();
		this.jcb_audio_device = new JComboBox();
		this.jLabel16 = new JLabel();
		this.jcb_audio_bitrate = new JComboBox();
		this.jLabel17 = new JLabel();
		this.jLabel18 = new JLabel();
		this.jcb_audio_channels = new JComboBox();
		this.jLabel19 = new JLabel();
		this.jcb_audio_sample_rate = new JComboBox();
		this.jLabel20 = new JLabel();
		this.js_video_bitrate = new JSpinner();
		this.jcb_audio_compensate = new JCheckBox();
		this.jb_vlc_path = new JButton();
		this.jb_start_broadcast = new JButton();
		this.jtf_output_field = new JTextField();
		this.jLabel21 = new JLabel();
		
		//this.jck_video_duplicate = new JCheckBox();

		setDefaultCloseOperation(3);
		setTitle("Streamer");
		setCursor(new Cursor(0));
		setIconImage(this.appIcon);
		setName("launcher");
		setMinimumSize(new Dimension(400,400));

		this.jLabel1.setFont(new Font("Tahoma", 0, 14));
		this.jLabel1.setText("VLC Streamer");

		this.jPanel1.setBorder(BorderFactory.createTitledBorder("Settings"));
		this.jPanel1.setMinimumSize(new Dimension(400,400));
		this.jPanel1.setLayout(new BorderLayout());

		this.jLabel2.setText("Presents:");

		this.jcb_presents.setModel(new DefaultComboBoxModel(new String[] { "Custom Quality", "--------------------------------", "Lowest SD Quality 4:3", "Lowest SD Quality 16:9", "Basic SD Quality 4:3", "Basic SD Quality 16:9", "Standard SD Quality 4:3", "Standard SD Quality 16:9", "High SD Quality 4:3", "High SD Quality 16:9", "Best SD Quality 4:3", "Best SD Quality 16:9" }));
		this.jcb_presents.setSelectedIndex(6);
		this.jcb_presents.setToolTipText("A set of presents for transmission");
		this.jcb_presents.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent evt) {
				StartupScreen.this.jcb_presentsItemStateChanged(evt);
			}
		});
		this.jLabel3.setFont(new Font("Tahoma", 3, 11));
		this.jLabel3.setText("Video");
/*		
		this.jck_video_duplicate.setText("Duplicate");
		this.jck_video_duplicate.setToolTipText("Check to duplicate video to VLC, disable to save cpu");
		this.jck_video_duplicate.setSelected(true);
*/
		this.jLabel4.setText("Aspect ratio:");

		this.jcb_video_ar.setModel(new DefaultComboBoxModel(new String[] { "---------", "4:3", "16:9" }));
		this.jcb_video_ar.setToolTipText("Aspect ratio of the video");
		this.jcb_video_ar.setEnabled(false);
		this.jcb_video_ar.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent evt) {
				StartupScreen.this.jcb_video_arItemStateChanged(evt);
			}
		});
		this.jLabel5.setText("Resolution:");

		this.jcb_video_resolution_presents.setModel(new DefaultComboBoxModel(new String[] { "Custom", "----------", "240x180", "320x240", "640x480" }));
		this.jcb_video_resolution_presents.setSelectedIndex(3);
		this.jcb_video_resolution_presents.setToolTipText("Some video resolution defaults");
		this.jcb_video_resolution_presents.setEnabled(false);
		this.jcb_video_resolution_presents.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent evt) {
				StartupScreen.this.jcb_video_resolution_presentsItemStateChanged(evt);
			}
		});
		this.jLabel6.setText("or");

		this.jtf_video_width.setText("320");
		this.jtf_video_width.setToolTipText("Video width");
		this.jtf_video_width.setEnabled(false);

		this.jLabel7.setText("x");

		this.jtf_video_height.setText("240");
		this.jtf_video_height.setToolTipText("Video height");
		this.jtf_video_height.setEnabled(false);
		this.jtf_video_height.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				StartupScreen.this.jtf_video_heightActionPerformed(evt);
			}
		});
		this.jLabel8.setText("Bitrate:");

		this.jLabel9.setText("kbps");

		this.jLabel10.setText("Device:");

		if (videoDeviceNames != null && !videoDeviceNames.isEmpty()) {
			this.jcb_video_device.setEditable(false);
			this.jcb_video_device.addItem("-------------------------");
			this.jcb_video_device.addItem("Default");
			this.jcb_video_device.addItem("No video");
			int index = 1;
			for (String name : videoDeviceNames) {
				if (!name.equals("none"))
					this.jcb_video_device.addItem(name);
			}

			this.jcb_video_device.setSelectedIndex(index);
			this.jcb_video_device.setToolTipText("The video source device, pick one of the list");
		} else {
			this.jcb_video_device.setEditable(true);
			this.jcb_video_device.setModel(new DefaultComboBoxModel(new String[] { "-------------------------", "Default", "No video" }));
			this.jcb_video_device.setSelectedIndex(1);
			this.jcb_video_device.setToolTipText("The video source device, pick one of the list or type the name of the device you want to use EXACTLY");
		}

		this.jLabel11.setText("Framerate:");

		this.jcb_video_framerate.setModel(new DefaultComboBoxModel(new String[] { "15", "20", "25", "30" }));
		this.jcb_video_framerate.setSelectedIndex(1);
		this.jcb_video_framerate.setToolTipText("Video frame rate, the lower the value the less fluid the video will look");
		this.jcb_video_framerate.setEnabled(false);

		this.jLabel12.setText("fps");

		this.jcb_video_codec.setModel(new DefaultComboBoxModel(new String[] { "h264", "mp4v","flv" }));
		this.jcb_video_codec.setToolTipText("Video codec, h264 needs more processing power but delivers better quality at lower bitrates");
		this.jcb_video_codec.setEnabled(false);

		this.jLabel13.setText("Codec:");

		this.jLabel14.setFont(new Font("Tahoma", 3, 11));
		this.jLabel14.setText("Audio");

		this.jLabel15.setText("Device:");

		if (audioDeviceNames != null && !audioDeviceNames.isEmpty()) {
			this.jcb_audio_device.setEditable(false);
			this.jcb_audio_device.addItem("-------------------------");
			this.jcb_audio_device.addItem("Default");
			this.jcb_audio_device.addItem("No sound");
			int index = 1;
			for (String name : audioDeviceNames) {
				if (!name.equals("none"))
					this.jcb_audio_device.addItem(name);
			}
			this.jcb_audio_device.setSelectedIndex(index);
			this.jcb_audio_device.setToolTipText("The audio source device, pick one of the list");
		} else {
			this.jcb_audio_device.setEditable(true);
			this.jcb_audio_device.setModel(new DefaultComboBoxModel(new String[] { "-------------------------", "Default", "No sound" }));
			this.jcb_audio_device.setSelectedIndex(1);
			this.jcb_audio_device.setToolTipText("The audio source device, pick one of the list or type the name of the device you want to use EXACTLY");
		}

		this.jLabel16.setText("Bitrate:");

		this.jcb_audio_bitrate.setModel(new DefaultComboBoxModel(new String[] { "32", "40", "48", "56", "64", "80", "96", "112", "128", "160", "192", "224", "256", "320" }));
		this.jcb_audio_bitrate.setSelectedIndex(3);
		this.jcb_audio_bitrate.setToolTipText("Audio bitrate, the higher the value the more quality the sound has but also more bandwidth is needed for transmission");
		this.jcb_audio_bitrate.setEnabled(false);

		this.jLabel17.setText("kbps");

		this.jLabel18.setText("Channels:");

		this.jcb_audio_channels.setModel(new DefaultComboBoxModel(new String[] { "Mono", "Stereo" }));
		this.jcb_audio_channels.setSelectedIndex(1);
		this.jcb_audio_channels.setToolTipText("The number of audio channels");
		this.jcb_audio_channels.setEnabled(false);

		this.jLabel19.setText("Frequency:");

		this.jcb_audio_sample_rate.setModel(new DefaultComboBoxModel(new String[] { "8000", "11025", "22050", "44100", "48000" }));
		this.jcb_audio_sample_rate.setSelectedIndex(3);
		this.jcb_audio_sample_rate.setToolTipText("Audio sample rate, higher values are better, lower than 22050 means reduced sound range");
		this.jcb_audio_sample_rate.setEnabled(false);

		this.jLabel20.setText("Hz");

		this.js_video_bitrate.setModel(new SpinnerNumberModel(200, 200, 600, 50));
		this.js_video_bitrate.setToolTipText("Video bitrate, the higher the bitrate, the more bandwidth necessary");
		this.js_video_bitrate.setEnabled(false);

		this.jcb_audio_compensate.setSelected(true);
		this.jcb_audio_compensate.setText("Compensate for No Sound");
		this.jcb_audio_compensate.setToolTipText("Click here to compensate the video bitrate when you select No Sound");
		this.jcb_audio_compensate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				StartupScreen.this.jcb_audio_compensateActionPerformed(evt);
			}
		});
		GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
		this.jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(
				jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addComponent(this.jLabel16)
										.addContainerGap(GroupLayout.DEFAULT_SIZE, 32767))
										.addComponent(this.jLabel19)
										.addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
												.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
														.addGroup(GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
																.addComponent(this.jLabel14, -2, 46, -2)
																.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 121, -2)
																.addComponent(this.jcb_audio_compensate))
																.addComponent(this.jLabel15, GroupLayout.Alignment.LEADING)
																.addGroup(GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
																		.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
																				.addComponent(this.jLabel18)
																				.addComponent(this.jLabel11)
																				.addComponent(this.jLabel13)
																				.addComponent(this.jLabel8)
																				.addComponent(this.jLabel10)
																				.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
																						.addComponent(this.jLabel3, GroupLayout.Alignment.LEADING, -1, -1, 32767)
																						.addComponent(this.jLabel2, GroupLayout.Alignment.LEADING, -1, -1, 32767))
																						.addComponent(this.jLabel5)
																						.addComponent(this.jLabel4))
																						.addGap(10, 10, 10)
																						.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
																								.addComponent(this.jcb_presents, GroupLayout.DEFAULT_SIZE, 300, GroupLayout.DEFAULT_SIZE)
																								.addGroup(jPanel1Layout.createSequentialGroup()
																										.addComponent(this.jcb_video_resolution_presents, -2, 114, -2)
																										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
																										.addComponent(this.jLabel6)
																										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
																										.addComponent(this.jtf_video_width, -2, 50, -2)
																										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
																										.addComponent(this.jLabel7)
																										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
																										.addComponent(this.jtf_video_height, -2, 50, -2))
																										.addComponent(this.jcb_video_ar, -2, 60, -2)
																										.addGroup(jPanel1Layout.createSequentialGroup()
																												.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
																														.addComponent(this.js_video_bitrate, GroupLayout.Alignment.LEADING)
																														.addComponent(this.jcb_video_framerate, GroupLayout.Alignment.LEADING, 0, -1, 32767))
																														.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
																														.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
																																.addComponent(this.jLabel9, -1, 195, 32767)
																																.addComponent(this.jLabel12)))
																																.addComponent(this.jcb_video_device, 0, 246, 32767)
																																.addComponent(this.jcb_video_codec, 0, 246, 32767)
																																.addComponent(this.jcb_audio_device, 0, 246, 32767)
																																.addGroup(jPanel1Layout.createSequentialGroup()
																																		.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
																																				.addComponent(this.jcb_audio_bitrate, 0, -1, 32767)
																																				.addComponent(this.jcb_audio_channels, 0, -1, 32767)
																																				.addComponent(this.jcb_audio_sample_rate, 0, -1, 32767))
																																				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
																																				.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
																																						.addComponent(this.jLabel17)
																																						.addComponent(this.jLabel20))
																																						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 163, 32767)))))
																																						.addGap(87, 87, 87)))));

		jPanel1Layout.setVerticalGroup(
				jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addComponent(this.jLabel2)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(this.jLabel3)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(this.jLabel10))
										.addGroup(jPanel1Layout.createSequentialGroup()
												.addComponent(this.jcb_presents, -2, -1, -2)
												.addGap(26, 26, 26)
												.addComponent(this.jcb_video_device, -2, -1, -2)
												.addGap(7, 7, 7)
												.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
														.addComponent(this.jcb_video_ar, -2, -1, -2)
														.addComponent(this.jLabel4))
														.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
														.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
																.addComponent(this.jcb_video_resolution_presents, -2, -1, -2)
																.addComponent(this.jLabel6)
																.addComponent(this.jtf_video_width, -2, -1, -2)
																.addComponent(this.jLabel7)
																.addComponent(this.jtf_video_height, -2, -1, -2)
																.addComponent(this.jLabel5))))
																.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
																		.addComponent(this.jLabel8)
																		.addComponent(this.js_video_bitrate, -2, -1, -2)
																		.addComponent(this.jLabel9))
																		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
																				.addComponent(this.jcb_video_framerate, -2, -1, -2)
																				.addComponent(this.jLabel11)
																				.addComponent(this.jLabel12))
																				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
																				.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
																						.addComponent(this.jLabel13)
																						.addComponent(this.jcb_video_codec, -2, -1, -2))
																						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
																						.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
																								.addComponent(this.jLabel14)
																								.addComponent(this.jcb_audio_compensate))
																								.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
																								.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
																										.addComponent(this.jLabel15)
																										.addComponent(this.jcb_audio_device, -2, -1, -2))
																										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
																										.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
																												.addComponent(this.jLabel16)
																												.addComponent(this.jcb_audio_bitrate, -2, -1, -2)
																												.addComponent(this.jLabel17))
																												.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
																												.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
																														.addComponent(this.jcb_audio_channels, -2, -1, -2)
																														.addComponent(this.jLabel18))
																														.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
																														.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
																																.addComponent(this.jcb_audio_sample_rate, -2, -1, -2)
																																.addComponent(this.jLabel19)
																																.addComponent(this.jLabel20))));

		try {
			this.jb_vlc_path.setIcon(new ImageIcon(getClass().getResource("/org/xistorias/gui/vlc.png")));
		} catch (NullPointerException e) {
			System.err.println("Could not locate: /org/xistorias/gui/vlc.png");
		}
		this.jb_vlc_path.setText("VLC");
		this.jb_vlc_path.setToolTipText("Click here to set the VLC path");
		this.jb_vlc_path.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				StartupScreen.this.jb_vlc_pathActionPerformed(evt);
			}
		});
		this.jb_start_broadcast.setFont(new Font("Tahoma", 1, 11));
		try {
			this.jb_start_broadcast.setIcon(new ImageIcon(getClass().getResource("/org/xistorias/gui/transmit.png")));
		} catch (NullPointerException e){
			System.err.println("Could not locate icon: transmit.png");
		}
		this.jb_start_broadcast.setText("Start Broadcast");
		this.jb_start_broadcast.setToolTipText("Click here to start broadcasting");
		this.jb_start_broadcast.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				StartupScreen.this.jb_start_broadcastActionPerformed(evt);
			}
		});
		this.jtf_output_field.setEditable(false);
		this.jtf_output_field.setText("Press Start Broadcast");
		this.jtf_output_field.setToolTipText("Output location");
		this.jtf_output_field.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				StartupScreen.this.jtf_output_fieldActionPerformed(evt);
			}
		});
		this.jLabel21.setText("Output:");

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(this.jPanel1, -2, 400, -2)
								.addComponent(this.jLabel1)
								.addGroup(layout.createSequentialGroup()
										.addComponent(this.jb_vlc_path)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 108, 32767)
										.addComponent(this.jb_start_broadcast, -2, 163, -2))
										.addComponent(this.jLabel21)
										.addComponent(this.jtf_output_field, GroupLayout.Alignment.TRAILING, -1, 342, 32767))
										.addContainerGap()));

		layout.setVerticalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(this.jLabel1)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(this.jPanel1, -2, -1, -2)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(this.jb_vlc_path)
								.addComponent(this.jb_start_broadcast))
								.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(this.jLabel21)
								.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(this.jtf_output_field, -2, -1, -2)
								.addContainerGap(-1, 32767)));

		pack();
	}

	private void jtf_video_heightActionPerformed(ActionEvent evt)
	{
	}

	private void jb_vlc_pathActionPerformed(ActionEvent evt) {
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showDialog(this, "Set VLC path");
		if (returnVal == 0)
			this.vlcPath = fc.getSelectedFile().getAbsolutePath();
		loadVLClib();
	}
	
	private void loadVLClib() {
		if (hasVlcLib) return;
		if (vlcPath == null) return;
		String libvlcPath = null;
		File vlc = new File(this.vlcPath);
		if (!vlc.exists()) {
			hasVlcLib = false;
			return;
		}
		try {
			switch(os) {
				case WINDOWS:
					libvlcPath = vlc.getParent();
					break;
				case LINUX:
					libvlcPath = "/usr/lib"; // safe bet I think
					break;
				case OSX:
					libvlcPath = vlc.getAbsolutePath()+"/Contents/MacOS/lib/";
					break;
				default:
					break;
			}
			if (libvlcPath == null) {
				hasVlcLib = false;
				return;
			}
			System.out.println("libname: "+RuntimeUtil.getLibVlcLibraryName());
			System.out.println("vlclib path: "+libvlcPath);
			if (os != OperatingSystem.WINDOWS) {
				NativeLibrary.addSearchPath(RuntimeUtil.getLibVlcLibraryName(), libvlcPath);
				System.out.println(Native.loadLibrary(RuntimeUtil.getLibVlcLibraryName(), LibVlc.class));
				hasVlcLib = true;
			}
		} catch (Exception e) {
			//e.printStackTrace();
			hasVlcLib = false;
		}
	}

	private void changeFieldEnableStatus(boolean status) {
		this.jcb_video_ar.setEnabled(status);
		this.jcb_video_resolution_presents.setEnabled(status);
		this.jtf_video_width.setEnabled(false);
		this.jtf_video_height.setEnabled(false);
		this.js_video_bitrate.setEnabled(status);
		this.jcb_video_framerate.setEnabled(status);
		this.jcb_video_codec.setEnabled(status);
		this.jcb_audio_bitrate.setEnabled(status);
		this.jcb_audio_channels.setEnabled(status);
		this.jcb_audio_sample_rate.setEnabled(status);
	}

	private void fillVideoResolutionPresents(String ar, String res) {
		this.jcb_video_resolution_presents.removeAllItems();
		if (ar.equals("4:3")) {
			this.jcb_video_resolution_presents.setModel(new DefaultComboBoxModel(new String[] { "Custom", "----------", "240x180", "320x240", "640x480" }));
			String aux = "240x180 320x240 640x480";
			if (aux.contains(res))
				this.jcb_video_resolution_presents.setSelectedItem(res);
			else
				this.jcb_video_resolution_presents.setSelectedIndex(0);
		} else {
			this.jcb_video_resolution_presents.setModel(new DefaultComboBoxModel(new String[] { "Custom", "----------", "240x135", "352x198", "720x405" }));
			String aux = "240x135 352x198 720x405";
			if (aux.contains(res))
				this.jcb_video_resolution_presents.setSelectedItem(res);
			else
				this.jcb_video_resolution_presents.setSelectedIndex(0);
		}
	}

	private void setPresent(String present) {
		setPresent(present, false);
	}

	private void setPresent(String present, boolean init) {
		if ((!init) && ((present.equals(this.currentPresent)) || (present.equals("--------------------------------")))) return;
		this.currentPresent = present;
		if (present.equals("Custom Quality")) {
			changeFieldEnableStatus(true);
			return;
		}
		changeFieldEnableStatus(false);
		Profile profile = (Profile)this.profiles.get(present);
		if (profile == null) return;

		this.jcb_video_ar.setSelectedItem(profile.video_aspectRatio);
		String res = profile.video_resolution_width + "x" + profile.video_resolution_height;
		fillVideoResolutionPresents(profile.video_aspectRatio, res);
		this.jtf_video_width.setText(""+profile.video_resolution_width);
		this.jtf_video_height.setText(""+profile.video_resolution_height);
		this.js_video_bitrate.setValue(Integer.valueOf(profile.video_bitrate));
		this.jcb_video_framerate.setSelectedItem(profile.video_framerate);
		this.jcb_video_codec.setSelectedItem(profile.video_codec);
		this.jcb_audio_bitrate.setSelectedItem(profile.audio_bitrate);
		this.jcb_audio_channels.setSelectedItem(profile.audio_channels == 2 ? "Stereo" : "Mono");
		this.jcb_audio_sample_rate.setSelectedItem(Integer.valueOf(profile.audio_samplerate));
	}

	private void jcb_presentsItemStateChanged(ItemEvent evt) {
		setPresent(this.jcb_presents.getSelectedItem().toString());
	}

	private void jcb_video_arItemStateChanged(ItemEvent evt) {
		String ar = this.jcb_video_ar.getSelectedItem().toString();
		if (ar.equals(this.currentAr)) return;
		this.currentAr = ar;
		fillVideoResolutionPresents(ar, ar.equals("4:3") ? "320x240" : "352x198");
	}

	private void jcb_video_resolution_presentsItemStateChanged(ItemEvent evt) {
		if ((this.jcb_video_resolution_presents == null) || (this.jcb_video_resolution_presents.getSelectedItem() == null)) return;
		String present = this.jcb_video_resolution_presents.getSelectedItem().toString();
		if ((present.equals(this.currentResolution)) || (present.equals("----------"))) return;
		this.currentResolution = present;
		if (present.equals("Custom")) {
			this.jtf_video_width.setEnabled(true);
			this.jtf_video_height.setEnabled(true);
			return;
		}
		this.jtf_video_width.setEnabled(false);
		this.jtf_video_height.setEnabled(false);
		String[] split = present.split("x");
		this.jtf_video_width.setText(split[0]);
		this.jtf_video_height.setText(split[1]);
	}

	private void jb_start_broadcastActionPerformed(ActionEvent evt) {
		if (this.vlcPath == null) {
			JOptionPane.showMessageDialog(new JDialog(), 
					"The path to the VLC executable is not set, please click the VLC button to set the path to the executable.", 
					"VLC not found", 
					0);
			return;
		}
		Vector<String> cmdArray = new Vector<String>();
		cmdArray.add(this.vlcPath);
		String execString = "\"" + this.vlcPath + "\" ";
		String present = this.jcb_presents.getSelectedItem().toString();
		Profile profile = (Profile)this.profiles.get(present);
		if ((profile == null) && (present.equals("Custom Quality")))
		{
			profile = new Profile(present);
			profile.video_aspectRatio = this.jcb_video_ar.getSelectedItem().toString();
			profile.video_resolution_width = new Integer(this.jtf_video_width.getText()).intValue();
			profile.video_resolution_height = new Integer(this.jtf_video_height.getText()).intValue();
			profile.video_bitrate = new Integer(this.js_video_bitrate.getValue().toString()).intValue();
			profile.video_framerate = new Integer(this.jcb_video_framerate.getSelectedItem().toString()).intValue();
			profile.video_codec = this.jcb_video_codec.getSelectedItem().toString();
			profile.audio_bitrate = new Integer(this.jcb_audio_bitrate.getSelectedItem().toString()).intValue();
			profile.audio_channels = (this.jcb_audio_channels.getSelectedItem().toString().equals("Stereo") ? 2 : 1);
			profile.audio_samplerate = new Integer(this.jcb_audio_sample_rate.getSelectedItem().toString()).intValue();
		}
		execString = execString + "-vvv -I qt --ttl 128 dshow:// ";
		cmdArray.add("-vvv");
		cmdArray.add("-I");
		cmdArray.add("qt");
		cmdArray.add("-ttl");
		cmdArray.add("128");
		cmdArray.add("dshow://");
		String videoDevice = this.jcb_video_device.getSelectedItem().toString();
		String audioDevice = this.jcb_audio_device.getSelectedItem().toString();
		if (!videoDevice.equals("No video"))
		{
			if ((!videoDevice.equals("Default")) && (!videoDevice.equals("-------------------------")))
				execString = execString + "--dshow-vdev=\"" + videoDevice + "\" ";
			execString = execString + "size=" + profile.video_resolution_width + "x" + profile.video_resolution_height + " ";
			cmdArray.add("--dshow-vdev=\"" + videoDevice + "\"");
			cmdArray.add("size=" + profile.video_resolution_width + "x" + profile.video_resolution_height);
			profile.video_enabled = true;
		} else {
			execString = execString + "--dshow-vdev=none ";
			profile.video_enabled = false;
		}
		if (audioDevice.equals("No sound")) {
			execString = execString + "--dshow-adev=none ";
			profile.audio_enabled = false;
		} else {
			profile.audio_enabled = true;
			if ((!audioDevice.equals("-------------------------")) && (!audioDevice.equals("Default")))
				execString += "--dshow-adev=\"" + audioDevice + "\" ";
		}
		execString += "--sout-mux-caching=50000 --sout=" + profile.getTranscodeString(this.jcb_audio_compensate.isSelected());
		execString += (duplicateDisplay ? ":duplicate{dst=http{dst=:8080/stream},dst=display}" : ":http{mux=ts,dst=:8080/stream}");
		System.out.println(execString);
		try {
			Runtime.getRuntime().exec(execString);
			System.exit(0);
		} catch (IOException ex) {
			Logger.getLogger(StartupScreen.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void jtf_output_fieldActionPerformed(ActionEvent evt)
	{
	}

	private void jcb_audio_compensateActionPerformed(ActionEvent evt)
	{
	}

	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run() {
				try {
					new StartupScreen(null).setVisible(true);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnsupportedLookAndFeelException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
}
