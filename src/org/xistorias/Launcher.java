/**
 * 
 */
package org.xistorias;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.UIManager.LookAndFeelInfo;

import org.xistorias.gui.StartupScreen;

/**
 * Application launcher.
 * Responsible by detecting Operating System and loading the correct Libraries. 
 * @author Miguel Gomes (mdg@uninova.pt)
 * @since 17/12/2014 16:05:05
 *
 */
public class Launcher
{
	/**
	 * Enumeration of supported Operationg Systems.
	 * @author Miguel Gomes (mdg@uninova.pt)
	 * @since 17/12/2014 16:09:32
	 *
	 */
	public enum OperatingSystem {
		UNKNOWN,
		WINDOWS,
		LINUX,
		OSX
	}
	
	/** Current Operating System */
	private static OperatingSystem os;
		
	/**
	 * Resolve Operating System.
	 */
	private static void initOperatingSystem() {
		String OS = System.getProperty("os.name").toLowerCase();
		String arch = System.getProperty("os.arch").toLowerCase();
		
		if (OS.indexOf("windows") >= 0) os = OperatingSystem.WINDOWS;
		else if (OS.indexOf("mac os x") >= 0) os = OperatingSystem.OSX;
		else if (OS.indexOf("linux") >= 0) os = OperatingSystem.LINUX;
		else os = OperatingSystem.UNKNOWN;
		
		if (os == OperatingSystem.WINDOWS) {
			System.out.println("Loading libray path");
			try {
				if (arch.equals("x86")) { // Load x86 lib
					System.loadLibrary("dsj_x86");
				} else { // Load x64 lib
					System.loadLibrary("dsj_x64");
				}
			} catch (UnsatisfiedLinkError e) {
				System.out.println("Loading libray from JAR ("+e.getLocalizedMessage()+")");
				try {
					if (arch.equals("x86")) { // Load x86 lib
						NativeUtils.loadLibraryFromJar("/dsj_x86.dll");
					} else { // Load x64 lib
						NativeUtils.loadLibraryFromJar("/dsj_x64.dll");
					}
				} catch (Exception e1) {
					// try to load from jar
					e1.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Gets environment variables.
	 * @return Properties
	 * @throws Throwable Exception
	 */
	private static Properties getEnvVars() throws Throwable
	{
		Process p = null;
		Properties envVars = new Properties();
		Runtime r = Runtime.getRuntime();
		String OS = System.getProperty("os.name").toLowerCase();

		if (OS.indexOf("windows 9") > -1)
			p = r.exec("command.com /c set");
		else if ((OS.indexOf("nt") > -1) || 
				(OS.indexOf("windows") > -1))
		{
			p = r.exec("cmd.exe /c set");
		}
		else {
			p = r.exec("env");
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		while ((line = br.readLine()) != null)
		{
			int idx = line.indexOf('=');
			String key = line.substring(0, idx);
			String value = line.substring(idx + 1);
			envVars.setProperty(key, value);
		}

		return envVars;
	}

	/**
	 * Gets a given environment variable by name
	 * @param name Variable name
	 * @return Variable value or null if there is no such variable
	 */
	public static String getEnv(String name) {
		Properties props = null;
		try {
			props = getEnvVars();
		} catch (Throwable e) {
			return null;
		}
		return props.getProperty(name);
	}

	/**
	 * Finds the VLC executable.
	 * @return VLC executable path or null if not found
	 */
	public static String findVLC() {
		String path = null;
		switch (os) {
			case WINDOWS:
				{
					path = getEnv("ProgramFiles(x86)");
					if (path == null)
						path = getEnv("ProgramFiles");
					// try to compose
					path = path + File.separator + "VideoLan" + File.separator + "VLC" + File.separator + "vlc.exe";
				}
				break;
			case OSX:
				path = "/Applications/VLC.app";
				break;
			case LINUX:
				path = "/usr/bin/vlc";
				break;
			default:
				break;
		}
		if (path == null) return path;
		File vlc = new File(path);
		if (vlc.exists()) return path;
		return null;
	}

	/**
	 * Starts the launcher.
	 * @param args Application arguments
	 */
	public static void main(String[] args)
	{
		initOperatingSystem();
		String vlcPath = findVLC();
		
		try
		{
			// Check what is the System look and feel
			String crossLaf = UIManager.getCrossPlatformLookAndFeelClassName();
			String sysLaf = UIManager.getSystemLookAndFeelClassName();
			String laf = sysLaf;
			if (sysLaf.equals(crossLaf)) { // try again
				String nimbusLaf = null;
				String gtkLaf = null;
				String windowsLaf = null;
				for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
					if (info.getName().equals("Nimbus")) {
						nimbusLaf = info.getClassName();
					} else if (info.getName().equals("GTK+")) {
						gtkLaf = info.getClassName();
					} else if (info.getName().equals("Windows")) {
						windowsLaf = info.getClassName();
					}
			    }
				switch (os) {
					case WINDOWS: laf = (windowsLaf != null ? windowsLaf : (nimbusLaf != null ? nimbusLaf : sysLaf)); break;
					case LINUX: laf = (gtkLaf != null ? gtkLaf : (nimbusLaf != null ? nimbusLaf : sysLaf)); break;
					case OSX:
					default:
						laf = (nimbusLaf != null ? nimbusLaf : sysLaf); break;
				}
			}
			UIManager.setLookAndFeel(laf); 
		} catch (Exception localException) {
			System.err.println("Exception: "+localException.getLocalizedMessage());
			try {
				UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedLookAndFeelException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		try {
			StartupScreen gui = new StartupScreen(vlcPath);
			gui.setVisible(true);
		} catch (Exception e) {
			javax.swing.JOptionPane.showMessageDialog(null, e.getLocalizedMessage(), "Exception", javax.swing.JOptionPane.ERROR_MESSAGE);
		}
	}
}
