# README #

This is a sample VLC configuration GUI to make streaming easier. 

### What is this repository for? ###

* VLC Streamer gui for simplified multiplatform streaming
* Version 1.0
* Windows, Linux and OSX supported

### How do I get set up? ###

* Java 1.5+
* Optionally some IDE (Eclipse/Netbeans/etc)
* Dependencies all included

### Contribution guidelines ###

* Write tests if necessary
* Code review
* Other guidelines

### Who do I talk to? ###

* If you have any questions ask.